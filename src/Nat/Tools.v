Require Import Nat.Def.
Require Import Nat.Add.

Fixpoint half (n: nat) : nat :=
  match n with
	| S (S p) => S (half p)
	| _ => Z
	end.

Definition even (n : nat) : Prop :=
  exists (p : nat), p + p = n.

Definition odd (n : nat) := ~(even n).

Fixpoint is_even (n : nat) : bool :=
  match n with
	| S (S p) => is_even p
	| S Z => false
	| Z => true
	end.

(*
Lemma half_sum: forall (a : nat), half a + half a = a \/ half a + half a = S a.
Proof.
Admitted.

Lemma even_half: forall (a : nat), is_even a = true -> half a + half a = a.
Proof.
Admitted.
*)

Lemma test_even : forall (a : nat), even a <-> is_even a = true.
Proof.
split; intros.
+ elim H; clear H; intros.
  generalize a H; clear a H.
	induction x; intros; rewrite <- H; trivial.
	simpl add; rewrite addComm; simpl.
	now apply IHx.
+

Admitted.
