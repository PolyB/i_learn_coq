Require Import Nat.Add.
(* ≤ *)
Definition le (n m : nat) := exists p, add n p = m.
Infix "≤" := le (at level 70, no associativity).

Definition lt (n m : nat) := le n (S m).
Infix "<" := lt (at level 70, no associativity).

Definition gt (n m : nat) := ~(le n m).
Infix ">" := gt (at level 70, no associativity).

Definition ge (n m : nat) := ~(le n (S m)).
Infix "≥" := ge (at level 70, no associativity).

Fixpoint le_test (n m: nat) : bool :=
  match (n, m) with
	| (Z, _) => true
	| (_, Z) => false
	| ((S o), (S p)) => le_test o p
	end.

Lemma leSaZ: forall a: nat, ~(le (S a) Z).
Proof.
intros a.
unfold le.
intro H.
elim H.
intros x Hx.
absurd (S a = Z).
discriminate.
apply (proj1 (addZ (S a) x)); assumption.
Qed.

Lemma leS: forall a b:nat, le a b -> le (S a) (S b).
Proof.
intros a b H.
elim H.
intros x Hx.
exists x.
simpl.
now f_equal.
Qed.

Lemma le_test_ok: forall n m, le n m <-> le_test n m = true.
split.

generalize m.
clear m.
induction n.
trivial.
intros m H.
case_eq m.
intro Hm.
exfalso.
rewrite Hm in H.
now apply (leSaZ n).
intros sm Hm.
simpl.
apply IHn.
rewrite Hm in H.
elim H.
intros x Hx.
exists x.
simpl in Hx.
now inversion Hx.

generalize m.
clear m.
induction n.
intros m.
now exists m.
intros m H.
case_eq m.
intro Hm.
rewrite Hm in H.
discriminate H.
intros sm Hm.
rewrite Hm in H.
simpl in H.
apply leS.
now apply IHn.
Qed.

(* ∀ n : n ≤ n *)
Lemma le_refl: forall n, le n n.
intro n.
unfold le.
exists Z.
apply addaZ.
Qed.

(* ∀ n m p: n≤m → m≤p -> n≤p *)
Lemma le_trans: forall n m p, le n m -> le m p -> le n p.
intros n m p.
unfold le.
intro H1.
intro H2.
elim H1.
elim H2.
intro x.
intro x0.
intro x1.
intro H3.
exists (add x1 x).
rewrite <- addAssoc.
rewrite H3.
rewrite x0.
trivial.
Qed.

Lemma le_antisym: forall a b : nat, (a ≤ b) -> (b ≤ a) -> a = b.
intros a b H I.
unfold le in H.
unfold le in I.
elim H.
intro x.
intro J.
elim I.
intro y.
intro K.
rewrite <- K in J.
rewrite addAssoc in J.
apply addaba in J.
apply addZ in J.
destruct J.
rewrite <- K.
rewrite H0.
apply addaZ.
Qed.
