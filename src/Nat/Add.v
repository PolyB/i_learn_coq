Require Export Nat.Def.

(* + *)
Fixpoint add (n m: nat) : nat :=
  match n with
    | Z => m
    | S p => S (add p m)
  end.
Infix "+" := add (at level 50, left associativity).
(* Z + a = a *)
Lemma addZa: forall a: nat, add Z a = a.
Proof.
reflexivity.
Qed.

(* a + Z = a *)
Lemma addaZ: forall a: nat, add a Z = a.
Proof.
intros A.
induction A.
trivial.
simpl add.
f_equal.
assumption.
Qed.

(* a + b = b + a *)
Lemma addComm: forall a b: nat, add a b = add b a.
Proof.
intros A B.
induction A.
rewrite addZa.
rewrite addaZ.
trivial.
simpl add.
rewrite IHA.
clear IHA.
induction B.
rewrite 2!addZa.
trivial.
simpl add.
f_equal.
trivial.
Qed.

(* a + b = 0 <-> a = 0 & b = 0 *)
Lemma addZ: forall a b: nat, add a b = Z <-> a = Z /\ b = Z.
intros a b.
split.
intro H.
split.
case b in H.


rewrite <- H.
rewrite addaZ;trivial.
rewrite addComm in H.
simpl in H.
discriminate H.

case a in H.

rewrite <- H.
rewrite addZa;trivial.
simpl in H.
discriminate H.

intro H1.
destruct H1.
rewrite H.
rewrite H0.
trivial.
Qed.
(* (a + b) + c = a + (b + c) *)
Lemma addAssoc: forall a b c: nat, add (add a b) c = add a (add b c).
Proof.
intros a b c.
induction a.
rewrite 2!addZa.
trivial.
simpl add.
f_equal.
trivial.
Qed.

Lemma addaba: forall a b: nat, a + b = a -> b = Z.
intros a b.
intro H.
induction a.
trivial.
apply IHa.
simpl in H.
inversion H.
f_equal.
apply H1.
Qed.
