Require Export Nat.Add.

(* * *)
Fixpoint mult (n m: nat) : nat :=
  match n with
    | Z => Z
    | S p => add m (mult p m)
  end.
Infix "×" := mult (at level 40, left associativity).
(* Z * a = Z *)
Lemma multZa: forall a : nat , mult Z a = Z.
Proof.
intros a.
simpl mult.
trivial.
Qed.

(* a * Z = Z *)
Lemma multaZ: forall a : nat, mult a Z = Z.
Proof.
intros a.
induction a.
trivial.
simpl mult.
rewrite IHa.
trivial.
Qed.

(* a * b = b * a *)
Lemma multComm: forall a b: nat, mult a b = mult b a.
Proof.
intros a b.
induction a.
rewrite multaZ.
apply multZa.
simpl mult.
rewrite IHa.
clear IHa.
induction b.
rewrite addZa.
rewrite multZa; trivial.
simpl add.
simpl mult.
f_equal.
rewrite <- IHb.
rewrite <- addAssoc.
replace (add a (add b (mult b a))) with (add (add a b) (mult b a)).
replace (add b a) with (add a b).
trivial.
apply addComm.
rewrite addAssoc.
trivial.
Qed.

(* a * (b + c) =  a*b + a*c *)
Lemma multDist: forall a b c: nat, mult a (add b c) = add (mult a b) (mult a c).
Proof.
intros a b c.
induction a.
trivial.
simpl mult.
rewrite IHa.
rewrite <- addAssoc.
rewrite (addAssoc b c (mult a b)).
rewrite (addComm c (mult a b)).
rewrite <- (addAssoc b (mult a b) c).
rewrite (addAssoc (add b (mult a b)) c (mult a c)).
trivial.
Qed.

(* a * (b *c) = (a*b) * c *)
Lemma multAssoc: forall a b c: nat, mult a (mult b c) = mult (mult a b) c.
Proof.
intros a b c.
induction a.
trivial.
simpl mult.
rewrite IHa.
clear IHa.
induction b.
trivial.
simpl add.
simpl mult.
rewrite addAssoc.
f_equal.
rewrite (multComm a (S b)).
simpl mult.
pose (d := (add a (mult b a))).
replace (add a (mult b a)) with d; try trivial.
rewrite multComm.
rewrite (multComm d c).
rewrite <- multDist.
rewrite multComm.
trivial.
Qed.
