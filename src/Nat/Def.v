(* natural positive integer *)
Inductive nat: Set :=
  | Z: nat
  | S: nat -> nat.

Fixpoint nat_eq_test (a b: nat) : bool :=
  match (a, b) with
  | (Z, Z) => true
  | (S c, S d) => nat_eq_test c d
  | _ => false
  end.

Lemma nat_eq_test_ok: forall (a b: nat), a = b <-> nat_eq_test a b = true.
Proof.
split.
intros H.
rewrite H.
clear H.
now induction b.

generalize b.
clear b.
induction a.
intros b H.
case_eq b.
trivial.
intros sb Hsb.
rewrite Hsb in H.
discriminate.
intros b H.
case_eq b.
intros Hb.
rewrite Hb in H.
discriminate.
intros sb Hsb.
f_equal.
rewrite Hsb in H.
simpl in H.
now apply IHa.
Qed.