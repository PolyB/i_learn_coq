Require Import L.Nat.Add.
Require Import L.Maybe.

(* - *)
Fixpoint sub (n m : nat) : maybe nat :=
  match (n, m) with
  | (S a, S b) => sub a b
  | (a, Z) => Just nat a
  | (Z, S _) => Nothing nat
  end.
Infix "-" := sub (at level 50, left associativity).
(* a - 0 = a *)
Lemma subaZ: forall n : nat, sub n Z = Just nat n.
intro n.
induction n; simpl sub;trivial.
Qed.

(* a - a = 0 *)
Lemma subaa:forall n : nat, sub n n = Just nat Z.
intro n.
induction n; simpl sub; trivial.
Qed.

(* (n + m) - m = n *)
Lemma nataddsub: forall n m, sub (add n m) m = Just nat n.
induction n.
intro m.
rewrite addZa.
apply subaa.
simpl add.
induction m.
simpl sub.
rewrite addaZ.
trivial.
simpl sub.
replace (add n (S m)) with (S (add  n m)).
apply IHm.
rewrite (addComm n (S m)).
simpl add.
rewrite addComm.
trivial.
Qed.

(* a -b = Nothing -> a - (b + 1) = Nothing *)
Lemma subaZN: forall a b : nat, sub a b = Nothing nat -> sub a (S b) = Nothing nat.
induction a.
trivial.
intro b.
intro H.
simpl.
case_eq b;intro H1.
rewrite H1 in H.
rewrite <- H.
rewrite subaZ.
rewrite subaZ.
f_equal.
discriminate.
intro H2.
rewrite IHa.
trivial.
rewrite <- H.
rewrite H2.
simpl.
trivial.
Qed.

(* 0 - a = b -> a = 0 & b = 0 *)
Lemma subZa: forall (a b : nat), (sub Z a) = Just nat b <-> a = Z /\ b = Z.
intros a b.
split.
intro H.
split.
induction a.
trivial.
inversion H.
induction b.
trivial.
inversion H.
induction a.
apply (just_eq nat) in H1.
discriminate H1.
discriminate H1.
intro H.
destruct H.
rewrite H.
simpl sub.
rewrite H0.
trivial.
Qed.

(* a - b = c <-> b + c = a *)
Lemma subeqadd: forall a b c: nat, sub a b = Just nat c <-> add b c = a.
split.
generalize a b c.
clear a b c.
induction a.
intros b c H.
apply subZa in H.
apply addZ.
trivial.
intros b c H.
induction b.
rewrite addZa.
simpl in H.
inversion H.
trivial.
simpl add.
f_equal.
apply IHa.
inversion H.
trivial.

intro H.
induction a.
apply addZ in H.
destruct H.
rewrite H.
rewrite H0.
trivial.
rewrite <- H.
rewrite addComm.
rewrite nataddsub.
trivial.
Qed.

(* a -b = 0 <-> a = b *)
Lemma subZ: forall a b: nat, sub a b = Just nat Z <-> a = b.
intros a b.
split.
intro H.
apply subeqadd in H.
rewrite addaZ in H.
rewrite H.
trivial.
intro H.
rewrite H.
apply subaa.
Qed.

(* a - (a + 1) = Nothing *)
Lemma subaSa: forall a : nat, sub a (S a) = Nothing nat.
intro a.
induction a.
trivial.
simpl sub.
trivial.
Qed.

(* a - (b + c) = (a - b) - c *)
Lemma subadd: forall a b c : nat, sub a (add b c) = maybejoin nat nat (fun x:nat => sub x c) (sub a b).
intro a.
case_eq a.
intro H.
intro b.
case_eq b.
trivial.
trivial.
intro n.
intro H.
generalize n.
induction b.
trivial.
clear H a n.
intro c.
replace (add (S b) c) with (add b (S c)).
rewrite IHb.
clear IHb.
case b.
simpl sub.
rewrite subaZ.
simpl maybejoin.
trivial.
intro n.
simpl sub.
case n0.
simpl.
case n; trivial.
intro n1.
simpl sub.
case n.
rewrite subaZ.
simpl maybejoin.
trivial.
intro n2.
clear n0 b n.
case_eq (sub n1 n2).
intro H.
apply subaZN in H.
rewrite H.
trivial.
intros n H.
simpl maybejoin.
case_eq n.
simpl sub.
intro H1.
rewrite H1 in H.
apply subZ in H.

rewrite H.
rewrite subaSa.
trivial.
intro n0.
intro H1.
apply subeqadd in H.
rewrite <- H.
rewrite addComm.
rewrite H1.
replace (add (S n0) n2) with (add n0 (S n2)).
rewrite nataddsub.
trivial.
simpl.
rewrite addComm.
simpl.
f_equal.
rewrite addComm.
trivial.
simpl.
rewrite addComm.
simpl.
f_equal.
rewrite addComm.
trivial.
Qed.
