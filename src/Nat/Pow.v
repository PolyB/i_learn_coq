Require Export Nat.Mult.

Fixpoint pow (n m : nat) : nat :=
  match m with
  | Z => S Z
  | S p => mult n (pow n p)
end.
Infix "^" := pow  (at level 30, right associativity).

Lemma powaZ: forall a : nat, pow a Z = S Z.
trivial.
Qed.

Lemma powZa : forall a : nat, a <> Z <->  pow Z a = Z.
split.
intros H.
induction a.
simpl.
absurd (Z = Z).
trivial.
trivial.
trivial.
intro H.
intro H0.
rewrite H0 in H.
simpl in H.
discriminate H.
Qed.

Lemma powUa : forall a : nat, pow (S Z) a = S Z.
intro a.
induction a.
trivial.
simpl.
rewrite addaZ.
trivial.
Qed.

Lemma powMultDistr: forall a b c : nat, pow (mult a b) c = mult (pow a c) (pow b c).
intros a b c.
induction c.
rewrite powaZ.
trivial.
simpl.
rewrite IHc.
rewrite <- 2!multAssoc .
f_equal.
rewrite (multComm (pow a c) (mult b (pow b c))).
rewrite <- multAssoc.
f_equal.
rewrite multComm.
reflexivity.
Qed.

Lemma powPlus: forall a b c : nat, pow a (add b c) = mult (pow a b) (pow a c).
intros a b c.
induction b.
simpl.
rewrite addaZ.
trivial.
simpl add.
simpl.
rewrite <- multAssoc.
f_equal.
assumption.
Qed. 

Lemma powMult : forall a b c : nat, pow (pow a b) c = pow a (mult b c).
intros a b c.
induction b.
rewrite powaZ.
rewrite powUa.
simpl mult.
rewrite powaZ.
trivial.
simpl.
rewrite powPlus.
rewrite <- IHb.
rewrite powMultDistr.
f_equal.
Qed.
