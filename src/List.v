Require Export Nat.Def.
Require Import Nat.Add.
Require Import Maybe.

Inductive list (A :Type) : Type :=
  | nil : list A
  | cons : A -> list A -> list A.

Arguments nil {A}.
Arguments cons {A}.
Infix ":" := cons (at level 60, right associativity).

Fixpoint len (A:Type) (l : list A) : nat :=
    match l with
    | nil => Z
    | cons _ x => S (len A x)
    end.

Arguments len {A}.

Fixpoint conc (A : Type) (l p : list A) :=
    match l with
    | nil => p
    | cons x xs => cons x (conc A xs p)
  end.

Arguments conc {A}.

Infix "++" := conc (right associativity, at level 60).

Fixpoint nth (A : Type) (l : list A) (i : nat) : maybe A :=
  match (l, i) with
  | (cons x _, Z) => Just A x
  | (cons _ xs, S n) => nth A xs n
  | (nil, _) => Nothing A
  end.

Arguments nth {A}.

Fixpoint find (A : Type)(p : A -> bool)(l : list A) : maybe A :=
  match l with
  | (x:xs) => if p x then Just A x else find A p xs
  | nil => Nothing A
  end.

Arguments find {A}.

Fixpoint filter (A : Type)(p : A -> bool)(l : list A) : list A :=
  match l with
    | (x:xs) => if p x then x:(filter A p xs) else filter A p xs
    | nil => nil
  end.

Arguments filter {A}.

Fixpoint sorted (A : Type) (ord: A -> A -> Prop) (l : list A) : Prop :=
  match l with
  | (x:xs) => match xs with
                | (y:_) => (ord x y) /\ (sorted A ord xs)
                | _ => True
              end
  | _ => True
  end.

Arguments sorted {A}.

Fixpoint lowest_rec (A : Type) (ord_test: A -> A -> bool) (l : list A) (low : A) : A :=
  match l with
  | (x:xs) => if (ord_test low x) then
                              lowest_rec A ord_test xs low else lowest_rec A ord_test xs x
  | _ => low
  end.

Definition lowest (A : Type) (ord_test: A -> A -> bool) (l : list A) :=
  match l with
  | (x:xs) => Just A (lowest_rec A ord_test xs x)
  | _ => Nothing A
  end.

Arguments lowest {A}.

(*
Fixpoint sort (A : Type) (ord_test: A -> A -> bool) (l : list A) : list A :=
  let a := lowest ord_test l in
    match a with
    | Just _ b => (b:(sort A ord_test (filter (fun x: A => (ord_test b x && ord_test x b)%bool) l)))
    | Nothing _ => nil
  end.

Arguments sort {A}.
*)

Fixpoint insert (A : Type) (ord_test: A -> A -> bool) (l: list A) (e: A) : list A :=
  match l with
  | x:xs => if ord_test e x then e:x:xs else x:(insert A ord_test xs e)
  | _ => e:nil
  end.

Arguments insert {A}.

Fixpoint sort (A : Type) (ord_test: A -> A -> bool) (l : list A) : list A :=
  match l with
  | x:xs => insert ord_test (sort A ord_test xs) x
  | _ => nil
  end.

Arguments sort {A}.

Lemma concAddlen: forall (A : Type) (l1 l2 : list A), len (conc l1 l2) = add (len l1) (len l2).
intros a b c.
induction b.
trivial.
simpl.
f_equal.
trivial.
Qed.

Lemma concAssoc: forall (A : Type) (l1 l2 l3 : list A), l1 ++ (l2 ++ l3) = (l1 ++ l2) ++ l3.
intros ? ? ? ?.
induction l1.
trivial.
simpl.
f_equal.
trivial.
Qed.

Lemma lenTh: forall (A : Type) (l : list A), nth l (len l) = Nothing A.
intros ? ?.
induction l.
trivial.
simpl.
trivial.
Qed.

Lemma sortedRem : forall (A : Type) (ord : A -> A -> Prop) (l : list A) (e : A), sorted ord (e:l) -> sorted ord l.
intros ? ? ? ? ?.
induction l.
trivial.
simpl in H.
destruct H.
apply H0.
Qed.

Lemma sortedFilter : forall (A : Type) (ord : A -> A -> Prop) (p : A -> bool) (l:list A), sorted ord l -> sorted ord (filter p l).
Admitted.

Lemma sortedInsert : forall (A : Type) (ord : A -> A -> Prop)
  (ord_test : A -> A -> bool),
  (forall (a b : A), ord a b <-> ord_test a b = true) -> forall (l : list A),
  sorted ord l -> forall (a: A), sorted ord (insert ord_test l a).
Proof.
intros.
trivial.
Admitted.

Lemma sort_ok : forall (A : Type) (ord : A -> A -> Prop)
  (ord_test : A -> A -> bool),
  (forall (a b : A), ord a b <-> ord_test a b = true) -> forall (l : list A),
  sorted ord (sort ord_test l).
Proof.
intros.
induction l; simpl.
trivial.
simpl.
now apply sortedInsert.
Qed.

