(* ? *)
Inductive maybe (A : Type) : Type :=
  | Nothing : maybe A
  | Just: A -> maybe A.

(* join on maybe *)
Fixpoint maybejoin (A B : Type) (f: A -> maybe B) (n : maybe A) : maybe B :=
  match (n) with
  | Nothing _ => Nothing B
  | Just _ a => f a
  end.

(* map on maybe *)
Fixpoint maybemap (A B : Type) (f: A -> B) (n : maybe A) : maybe B :=
  match (n) with
  | Nothing _ => Nothing B
  | Just _ a => Just B (f a)
  end.

Definition just_eq: forall (T : Type) (a b : T), Just T a = Just T b <-> a = b.
split;intro H; inversion H; trivial.
Qed.
